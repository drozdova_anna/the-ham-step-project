const specialistsData = {
  "HASAN ALI": {
    about: "Integer dignissim, augue tempus ultricies luctus, quam dui laoreet sem, non dictum odio nisi quis massa. Morbi pulvinar odio eget aliquam facilisis. Tempus ultricies luctus, quam dui laoreet sem, non dictum odio nisi quis massa. Morbi pulvinar odio eget aliquam facilisis.",
    specialization: "UX Designer",
    photo: `./img/section/What People Say About theHam/Layer 5.png`
  },
  "JANNET PINK": {
    about: "ser experience (UX) design is the process design teams use to create products that provide meaningful and relevant experiences to users. This involves the design of the entire process of acquiring and integrating the product, including aspects of branding, design, usability and function.",
    specialization: "BACKEND DEVELOPER",
    photo: "./img/section/What People Say About theHam/Layer 6.png"
  },
  "FIBO HUGA": {
    about: "“No product is an island. A product is more than the product. It is a cohesive, integrated set of experiences. Think through all of the stages of a product or service – from initial intentions through final reflections, from first usage to help, service, and maintenance. Make them all work together seamlessly.”",
    specialization: "FRONTEND DEVELOPER",
    photo: "./img/section/What People Say About theHam/Layer 7.png",
  },
  "MARRET BRANTEN": {
    about: "These tasks can vary greatly from one organization to the next, but they always demand designers to be the users’ advocate and keep the users’ needs at the center of all design and development efforts. That’s also why most UX designers work in some form of user-centered work process, and keep channeling their best-informed efforts until they address all of the relevant issues and user needs optimally.",
    specialization: "UI DESIGNER",
    photo: "./img/section/What People Say About theHam/Layer 8.png",
  },
};


let currentSpecialistDiv = document.querySelector(".activeSpecialistInformation");
const specialistDataText = document.querySelector(".aboutTheHamSpecialistData");
const specialistName = document.querySelector(".aboutTheHamSpecialistName");
const specialistPosition = document.querySelector(".aboutTheHamSpecialistPosition");
const bigPictureCircle = document.querySelector(".aboutTheHamCircle");
const bigSpecialistPicture = document.querySelector(".aboutTheHamCircle > img");
const sliderTabListener = document.querySelector(".aboutTheHamspecialistPhotoSlider");



const animate = (text, name, photo, position, circle, timer) => {
  setTimeout(() => {
    text.classList.add("animateTextDisplayNew");
    name.classList.add("animateTextDisplayNew");
    photo.classList.add("animateTextDisplayNew");
    position.classList.add("animateTextDisplayNew");
    circle.classList.add("animateTextDisplayNew");
  }, timer);
}

const deleteAnimationClassAtClick = () => {
  bigPictureCircle.classList.remove?.("animateTextDisplayNew");
  specialistDataText.classList.remove?.("animateTextDisplayNew");
  specialistName.classList.remove?.("animateTextDisplayNew");
  specialistPosition.classList.remove?.("animateTextDisplayNew");
  bigSpecialistPicture.classList.remove?.("animateTextDisplayNew");
}

const changeActiveSpecialist = (specialistObjInfo, eventTarget) => {
  deleteAnimationClassAtClick();
  currentSpecialistDiv.classList.remove("activeSpecialistInformation");
  currentSpecialistDiv = eventTarget;
  currentSpecialistDiv.classList.add("activeSpecialistInformation");
  animate(specialistDataText, specialistName, specialistPosition, bigSpecialistPicture, bigPictureCircle, 0);
  setTimeout(() => {
    let currentSpecialistImg = document.querySelector(".activeSpecialistInformation > img").dataset.name;
    specialistDataText.innerText = `${specialistObjInfo[currentSpecialistImg].about}`;
    specialistName.innerText = `${currentSpecialistImg}`;
    specialistPosition.innerText = `${specialistObjInfo[currentSpecialistImg].specialization}`;
    bigSpecialistPicture.setAttribute("src", `${specialistObjInfo[currentSpecialistImg].photo}`);
  }, 700);
}

sliderTabListener.addEventListener("click", (e) => {
  if (e.target.closest(".aboutTheHamspecialistPhotoCircle") !== currentSpecialistDiv && e.target.closest(".aboutTheHamspecialistPhotoCircle")) {
    changeActiveSpecialist(specialistsData, e.target.closest("div"));
  }
})

document.addEventListener("DOMContentLoaded", () => {
  changeActiveSpecialist(specialistsData, document.querySelector(".activeSpecialistInformation"));
})


const getNextElement = (objectData, event) => {
  const currentElementDataKey = currentSpecialistDiv.children[0].dataset.name;
  const ObjKey = Object.keys(objectData);
  const maxLength = ObjKey.length - 1;
  if (event === "slideRight") {
    let nextElement = ObjKey.indexOf(`${currentElementDataKey}`) + 1;
    if (nextElement > maxLength) {
      nextElement = 0;
      return ObjKey[nextElement];
    }
    return ObjKey[nextElement];
  }
  let prevElement = ObjKey.indexOf(`${currentElementDataKey}`) - 1;
  if (prevElement < 0) {
    prevElement = maxLength;
    return ObjKey[prevElement];
  }
  return ObjKey[prevElement];
}


const sliderParentDiv = (nextElement) => {
  Array.from(document.querySelectorAll(`.aboutTheHamspecialistPhotoCircle > img`)).find(element => {
    if (element.dataset.name === nextElement) {
      currentSpecialistDiv = element.parentNode;
    }
  })
}


sliderTabListener.addEventListener("click", (e) => {
  e.preventDefault();
  deleteAnimationClassAtClick();
  if (e.target.closest(".swipeSlider")) {
    let action = e.target.closest("div.swipeSlider").classList[1];
    currentSpecialistDiv.classList.remove("activeSpecialistInformation");
    const nextElement = getNextElement(specialistsData, action);
    sliderParentDiv(nextElement);
    currentSpecialistDiv.classList.add("activeSpecialistInformation");
    let currentSpecialistImg = document.querySelector(".activeSpecialistInformation > img").dataset.name;
    animate(specialistDataText, specialistName, specialistPosition, bigSpecialistPicture, bigPictureCircle, 0);
    setTimeout(() => {
      specialistDataText.innerText = `${specialistsData[currentSpecialistImg].about}`;
      specialistName.innerText = `${currentSpecialistImg}`;
      specialistPosition.innerText = `${specialistsData[currentSpecialistImg].specialization}`;
      bigSpecialistPicture.setAttribute("src", `${specialistsData[currentSpecialistImg].photo}`);
    }, 700);
  }
});
