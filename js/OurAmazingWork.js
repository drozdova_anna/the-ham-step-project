function switchTabs () {

    const tabs = document.querySelector(".OurAmazingWork_nav");

    tabs.addEventListener('click', (event) => {
        cleaner();

        event.target.classList.add("OurAmazingWork_nav-item--active");
        let tabClass = event.target.innerHTML.replace(" ", "_");

        const galeryItems = document.querySelectorAll(".galleryItem");

        if ( tabClass === "All" ){
            galeryItems.forEach( elem => {
                elem.classList.remove("hide");
            })
        } else {
            galeryItems.forEach( elem => {
                if ( !elem.classList.contains(`${tabClass}`) ) {
                    elem.classList.add("hide");
                }
            })
        }
    })
}

function cleaner () {
    const tabsArray = [...document.querySelectorAll(".OurAmazingWork_nav-item")];
    tabsArray.forEach( elem => {

        if (elem.classList.contains("OurAmazingWork_nav-item--active")){
            elem.classList.remove("OurAmazingWork_nav-item--active");
        } 
    })

    const galeryItems = document.querySelectorAll(".galleryItem");
    galeryItems.forEach( elem => {
        elem.classList.remove("hide");
    })
}




function loadMore () {
    const btnLoadMore = document.querySelector(".OurAmazingWork_loadMoreBTN");
    const hiddenImages = document.querySelectorAll(".hide_loadMore");
    
    btnLoadMore.addEventListener('click', () => {
        hiddenImages.forEach( elem => {
            elem.classList.remove("hide_loadMore");
        })
        btnLoadMore.classList.add("hide_loadMoreBTN");
    })

}


switchTabs();
loadMore();