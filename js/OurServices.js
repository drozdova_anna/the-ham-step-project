function switchTabs() {

    const tabs = document.querySelector(".tabs");

    tabs.addEventListener('click', (event) => {
        const tabsArray = [...document.querySelectorAll(".tabs-title")];

        tabsArray.forEach(elem => {

            if (elem.classList.contains("tabs-title-active")) {
                elem.classList.remove("tabs-title-active");

                let tabClass = elem.innerHTML.replace(" ", "_");
                document.querySelector(`#${tabClass}-content`).classList.add("hide");
            }
        })

        event.target.classList.add("tabs-title-active");
        let tabClass = event.target.innerHTML.replace(" ", "_");

        const characterText = document.querySelector(`#${tabClass}-content`);
        characterText.classList.remove("hide");

    })
}

switchTabs();
